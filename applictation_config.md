# Application Configuration


We will be using Docker, which essentially are containers, so microservices are running within them rather than Virtual Machines and being resource heavy.  If you want to know more about Docker then click [here. ](https://www.youtube.com/watch?v=Qw9zlE3t8Ko)

### Details

Ensure that the latest version of node.js and npm is on your environment.  

An example of a docker file

    FROM node:8
    ARG VERSION=1.0.0
    # Copy the application files
    WORKDIR /usr/src/'name of your session'
    COPY . /usr/src/'name of your session'/
    LABEL license=GPL \
    version=$VERSION
    # Set required environment variables
    ENV NODE_ENV production
    # Download the required packages for production
    RUN npm update
    RUN npm install
    # Make the application run when running the container
    CMD ["node", "server.js"]


From Terminal/Command line

    $docker build -t ["insert name"] --build-arg <VERSION=1.0.0> -f Dockerfile

    $docker run -d --["insert name"] -p 3000:3000

    $docker start ["insert name"]

This is what we used to initially set up docker.  The ongoing plan is to use docker compose for future for multi containers so we can use the above commands can be completed in one step via a script.
