# Jenkins Integration

### Current status

1. Dev commits code

2. Committed code is, currently manually, pulled and wrapped in a dockerfile.

3. The wrapper code is pushed to the bitbucket account.

4. SSH into server

5. The code is pulled onto the server.

6. Docker is used to build and run the code.

Going forward, we will be using Jenkins a free automation server which can be used to trigger tests for the code. If the code passes the unit tests then the code is incorporated into building the application. If the code does not pass the unit tests the application is not built and the developer informed to amend and recommit the code.


Further forward, next steps for automation

A. Add Dockercompose yml file alongside the docker file.

B. (Currently the code is pulled from bitbucket by Jenkins.
However this is not currently running inside a docker container.)

C. 3/4 server implementation, lack of experience/knowledge.

D. Move away from the Jenkins UI to just the Jenkinsfile which would contain all the configuration.
